const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const textureManagerSchema = new Schema(
  {
    textureName: String,
    country: String,
    mainType: String,
    subType: String,
    fileList: [
      {
        textureFilename: String,
        border: { N: Number, S: Number, E: Number, W: Number },
        fileType: String,
        date: Number,
        month: Number,
        year: Number,
        legendFilename: String,
      },
    ],
  },
  { timestamps: true, versionKey: false }
);
const textureManagerModel = mongoose.model(
  "TextureManager",
  textureManagerSchema
);

module.exports = textureManagerModel;
