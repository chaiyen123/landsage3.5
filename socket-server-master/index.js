const express = require('express');
const cors = require('cors');
const axios = require('axios');
const fs = require('fs');
const schedule = require('node-schedule');

const PORT = process.env.PORT || 8080

const events = {
    mapCamera: "MAP_CAMERA",
    mapZoom: "MAP_ZOOM",
    mapTerrain: "MAP_TERRAIN",
    send_mapCamera: "SEND_MAP_CAMERA",
    send_mapZoom: "SEND_MAP_ZOOM",
    send_mapTerrain: "SEND_MAP_TERRAIN",
    sync: "SYNC",
    send_sync: "SEND_SYNC",
    onClickMarker: "ON_CLICK_MARKER",
    send_onClickMarker: "SEND_ON_CLICK_MARKER",
    openDataDetail: "OPEN_DATA_DETAIL",
    send_openDataDetail: "SEND_OPEN_DATA_DETAIL",
    changeDateXRangeSlider: "CHANGE_DATE_XRANGE_SLIDER",
    send_changeDateXRangeSlider: "SEND_CHANGE_DATE_XRANGE_SLIDER",
    changeDateRange: "CHANGE_DATE_RANGE",
    send_changeDateRange: "SEND_CHANGE_DATE_RANGE",
    selectDataLayer: "SELECT_DATA_LAYER",
    send_selectDataLayer: "SEND_SELECT_DATA_LAYER",
    textureSelected: "TEXTURE_SELECTED",
    send_textureSelected: "SEND_TEXTURE_SELECTED",
    countrySelected: "COUNTRY_SELECTED",
    send_countrySelected: "SEND_COUNTRY_SELECTED",
    beaconSelected: "BEACON_SELECTED",
    send_beaconSelected: "SEND_BEACON_SELECTED"
};

const siteProfiles = ['GLOBAL', 'TH', 'VN', 'MM', 'KH']

var app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.get('/', (req, res, next) => {
    res.send(200);
})

app.get('/nrth', async(req, res, next) => {
    const resp = await axios.get('https://api.mrcmekong.org/api/v1/nrtm/station')
    if (resp.data) {
        const data = resp
            .data
            .map(item => {
                return {
                    id: item.stationId,
                    position: [
                        item.latitude, item.longitude
                    ],
                    ...item
                }
            })
        res.send(data)
    } else {
        res.send([])
    }
})

var httpServer = require('http').createServer(app);
httpServer.listen(PORT, function () {
    console.log('rama-socket-server running on port ' + PORT + '.');
});

const io = require('socket.io')(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});
io.on('connection', function (socket) {
    socket.join(siteProfiles[0]);
    socket.room = siteProfiles[0];
    socket.on('JOIN', function (data) {
        console.log(`JOIN:`, data);
        socket.leave(socket.room);
        socket.join(data);
        socket.room = data;
        //broadcasting nrt data to client
        schedule.scheduleJob('*/5 * * * * *', async() => {
            const rawDataRF = fs.readFileSync('rainfall_nrt.json');
            const rainfall_nrt = JSON.parse(rawDataRF);
            socket
                .to(socket.room)
                .emit(`rainfall_nrt`, rainfall_nrt);

            const rawDataHD = fs.readFileSync('hydro_nrt.json');
            const hydro_nrt = JSON.parse(rawDataHD);
            socket
                .to(socket.room)
                .emit(`hydro_nrt`, hydro_nrt);
        });
    });

    Object
        .keys(events)
        .filter(item => item.indexOf('send_') < 0)
        .forEach(item => {
            socket
                .on(events[item], function (data) {
                    console.log(`${events[item]}:`, data);
                    socket
                        .to(socket.room)
                        .emit(events[`send_${item}`], data);
                });
        });
});

const interval = '0 0 */1 * * *'

schedule.scheduleJob(interval, async() => {
    const resp = await axios.get('https://api.mrcmekong.org/v1/time-series/rainfall/recent')
    if (resp.data) {
        const data = resp
            .data
            .features
            .map(item => {
                return {
                    id: item.properties.stationId,
                    position: [
                        item.geometry.coordinates[1], item.geometry.coordinates[0]
                    ],
                    ...item.properties
                }
            })
        fs.writeFileSync('rainfall_nrt.json', JSON.stringify(data));
    }
});

schedule.scheduleJob(interval, async() => {
    const resp = await axios.get('https://api.mrcmekong.org/api/v1/nrtm/station')
    if (resp.data) {
        const data = resp
            .data
            .map(item => {
                return {
                    id: item.stationId,
                    position: [
                        item.latitude, item.longitude
                    ],
                    ...item
                }
            })
        fs.writeFileSync('hydro_nrt.json', JSON.stringify(data));
    }
});