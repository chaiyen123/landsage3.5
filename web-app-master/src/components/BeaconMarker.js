import React, {Component} from 'react'
import {Marker, Popup} from 'react-leaflet';
import {Divider} from 'antd';
import {Row, Col} from 'react-bootstrap';
import Leaflet from 'leaflet';


const dischargeIcon = (fill) => new Leaflet.divIcon({
    html: `<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24">
        <g>
            <path xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_2" d="m0,11.86945l12.49178,-11.86945l12.49178,11.86945l-12.49178,11.86945l-12.49178,-11.86945z" fill="${fill}"/>
        </g>
        </svg>`,
    className:'',
    iconSize: [
        25, 24
    ],
    iconAnchor: [
        12.5, 24
    ],
    popupAnchor: [0, -28]
});

export const BeaconMarker = ({
    position,
    dateFiltered,
    timeSeriesData,
    id,
    onClick,
    markerRef,
    subTypes,
    isUnSync,
    data,
    emitter,
    detailKeys,
    vizPath,
    icon,
    name
}) => {
    const stationValue = timeSeriesData.find(item => item.Date === dateFiltered) || timeSeriesData[timeSeriesData.length - 1]
    const date = stationValue.Date, value = stationValue[subTypes]
    return (
        <Marker position={position} icon={icon} eventHandlers={onClick} ref={markerRef}>
            <Popup>
                <div
                    style={{
                    width: 280,
                    height: 'auto'
                }}>
                    <h6>{`Station : ${name}`}</h6>
                    {detailKeys.map(item => {
                        return (
                            <Row>
                                <Col xs={4}>
                                    <div
                                        style={{
                                        fontWeight: 'bold'
                                    }}>{`${item}:`}</div>
                                </Col>
                                <Col>
                                    <div
                                        style={{
                                        marginLeft: 4
                                    }}>{`${data[item]}`}</div>
                                </Col>
                            </Row>
                        )
                    })}
                    {data.fileList
                        ? (
                            <div>
                                <Divider/>
                                <div>{'File List'}</div>
                                {data
                                    .fileList
                                    .map(item => {
                                        return (
                                            <Row>
                                                <Col>
                                                    <a
                                                        style={{wordWrap: 'break-word',color:'#693'}}
                                                        onClick={() => {
                                                        window.open(`http://localhost:5000/data/${item.fileType}/${item.fileName}`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                                    }}>{item.fileName}</a>
                                                </Col>
                                                <Col style={{marginTop:12}}>
                                                    <a
                                                        href="javascript:void(0)"
                                                        style={{marginTop:12}}
                                                        onClick={() => {
                                                        window.open(`http://localhost:3001/${vizPath}/${item.fileName}`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                                        if (isUnSync) 
                                                            return;
                                                        emitter();
                                                    }}>{'More Details'}</a>
                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </div>
                        )
                        : null}
                </div>
            </Popup>
        </Marker>
    )
}