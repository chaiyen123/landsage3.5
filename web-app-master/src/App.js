import React, { useState, Component } from 'react'
import Leaflet from 'leaflet';
import {RemoveScrollBar} from 'react-remove-scroll-bar';
import {
    MapContainer,
    TileLayer,
    Marker,
    Popup,
    useMapEvent,
    LayersControl,
    Pane
} from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import io from 'socket.io-client'
import {
    Button,
    Table,
    Dropdown,
    Container,
    Row,
    Col,
    DropdownButton,
    Navbar, Nav, NavDropdown, Form, FormControl
} from 'react-bootstrap';
import { DropdownSubmenu, NavDropdownMenu} from "react-bootstrap-submenu";
import moment from 'moment';
import {Checkbox, Divider, Drawer, Slider} from 'antd';
import axios from 'axios';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/Home'
import Dashboard from './components/Dashboard';
import LogInDemo from './components/LoginDemo';
import SignUp from './components/SignUp';
import { AuthProvider } from './components/Auth';
import {DataMarker} from './components/DataMarker'
import {BeaconMarker} from './components/BeaconMarker'
import Scrollspy from 'react-scrollspy'
import Draggable from 'react-draggable';
import {Resizable, ResizableBox} from 'react-resizable';
import logo from "./LandSage-logo.webp";
import './App.css'
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-slider/dist/css/bootstrap-slider.css"
import 'bootstrap/dist/css/bootstrap.min.css';

import {
    dateRange,
    DATA_LAYER_LIST,
    openFullscreen,
    closeFullscreen,
    BEACON_LIST,
    TEXTURE_LIST,
    getBeaconData,
    getTextureData
} from './helper'
import ColumnGroup from 'antd/lib/table/ColumnGroup';

const satelliteUrl = `https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}`
const normalUrl = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`

const socket = io('http://localhost:8080')
//const socket = io('http://localhost:8080')
const EVENT_LIST = [
    'Location',
    'Data Marker Click',
    'Sync State',
    'Data Layer',
    'Texture Layer',
    'Country',
    'Date Slider'
]
const dateRangeDummy = dateRange();
const SITE = [
    {
        short: 'GLOBAL',
        title: 'GLOBAL'
    }, {
        short: 'TH',
        title: 'THAILAND'
    }, {
        short: 'VN',
        title: 'VIETNAM'
    }, {
        short: 'LA',
        title: 'LAOS'
    }, {
        short: 'KH',
        title: 'CAMBODIA'
    }
]
const AREA_FILTER = ['TH', 'VN', 'LA', 'KH', 'MRB']
const legends_beacon = {
    Rainfall: require('./asset/Rainfall_Legend.png').default,
    WaterLevel: require('./asset/WaterLevel_Legend.png').default,
    Discharge: require('./asset/Discharge_Legend.png').default
}

export default class MapDisplay extends Component {
    state = {
        lat: 15.2447,
        lng: 104.8475,
        zoom: 7,
        isSync: true,
        currentSite: 'GLOBAL',
        currentDateRangeValue: dateRangeDummy.length - 1,
        currentDateRange: dateRangeDummy[dateRangeDummy.length - 1],
        availableDataLayerKey: {},
        availableBeaconLayerKey: {},
        availableCountryFilterKey: {
            TH: true,
            VN: true,
            LA: true,
            KH: true,
            MRB: true
        },
        availableLegendKey: {},
        hideLegendKey: {},
        dataLayerList: {},
        beaconLayerList: {},
        unSyncKey: {},
        isShowDataLayerPanel: false,
        isShowCountryFilterPanel: false,
        isShowSyncProfilePanel: false,
        isShowBeaconPanel: false,
        isShowTexturePanel: false,
        isFullScreen: false,
        isShowBottomDrawer: false,
        activeDrags: false,
        dateFiltered: moment(),
        beaconLists: [],
        beaconLayers: {},
        textureList: [],
        textureLayers: {},
        overlays: [],
        baseMap: normalUrl
    }
    mapRef = React.createRef()
    markerRefs = []

    componentDidMount = async() => {
        this.initialSocketEvent();
        const beaconLists = await BEACON_LIST();
        const textureList = await TEXTURE_LIST();
        this.setState({beaconLists, textureList});

    }

    initialSocketEvent() {
        const {unSyncKey} = this.state
        console.log('unSyncKey', unSyncKey);

        socket.on("connect", () => {
            console.log(socket.id);
            socket.emit('JOIN', this.state.currentSite);
        });
        socket.on('SEND_MAP_CAMERA', data => {
            if (unSyncKey['Location']) 
                return;
            
            //console.log('SEND_MAP_CAMERA', data)
            const center = {
                lat: data.lat,
                lng: data.lng
            }
            this.setState({lat: data.lat, lng: data.lng})
            if (this.state.isSync) {
                this
                    .mapRef
                    .current
                    .setView(center, this.state.zoom)
            }

        })
        socket.on('SEND_MAP_ZOOM', data => {
            if (unSyncKey['Location']) 
                return;
            
            //console.log('SEND_MAP_ZOOM', data)
            this.setState({zoom: data.zoom, lat: data.center.lat, lng: data.center.lng})
            if (this.state.isSync) {
                this
                    .mapRef
                    .current
                    .setView(data.center, data.zoom)
            }
        })
        socket.on('SEND_SYNC', data => {
            if (unSyncKey['Sync State']) 
                return;
            this.setState({isSync: data})
        })
        socket.on('SEND_ON_CLICK_MARKER', data => {
            if (unSyncKey['Data Marker Click']) 
                return;
            this.setState({isSync: data})
        })
        socket.on('SEND_CHANGE_DATE_RANGE', data => {
            if (unSyncKey['Date Slider']) 
                return;
            const {dateFiltered} = this.state;
            const year = dateFiltered.year();
            const newYear = moment(data).year();
            console.log('SEND_CHANGE_DATE_RANGE', data);
            this.setState({
                dateFiltered: moment(data)
            }, () => {
                if (year != newYear) {
                    this.updateTextureOverlay();
                }
                this.updateBeacon();
            })
        })
        socket.on('SEND_SELECT_DATA_LAYER', async data => {
            if (unSyncKey['Data Layer']) 
                return;
            const {availableDataLayerKey, dataLayerList} = this.state
            const item = DATA_LAYER_LIST.find(obj => obj.name === data.name);
            if (data && data.value) {
                availableDataLayerKey[data.name] = data.value
                this.setState({availableDataLayerKey})
                if (item.type === 'overlay') {
                    this.toggleImageOverlay(item);
                } else {
                    const dataRes = await item.function ();
                    dataLayerList[data.name] = dataRes
                    this.setState({dataLayerList})
                }
            } else {
                if (item.type === 'overlay') {
                    this.toggleImageOverlay(item);
                }
                delete dataLayerList[data.name]
                delete availableDataLayerKey[data.name]
                this.setState({dataLayerList, availableDataLayerKey})
            }
        })
        socket.on('SEND_BEACON_SELECTED', async data => {
            if (unSyncKey['Data Layer']) 
                return;
            const {availableBeaconLayerKey, dateFiltered, beaconLists, beaconLayerList} = this.state
            const item = beaconLists.find(obj => obj.name === data.name);
            console.log('item', item);
            if (data && data.value) {
                availableBeaconLayerKey[data.name] = data.value
                this.setState({
                    availableBeaconLayerKey
                }, async() => {
                    this.updateBeacon();
                })
            } else {
                delete beaconLayerList[data.name]
                delete availableBeaconLayerKey[data.name]
                this.setState({beaconLayerList, availableBeaconLayerKey})
            }
        })
        socket.on('SEND_TEXTURE_SELECTED', data => {
            if (unSyncKey['Texture Layer']) 
                return;
            const {availableDataLayerKey} = this.state;
            if (data && data.value) {
                availableDataLayerKey[data.name] = data.value
                this.setState({
                    availableDataLayerKey
                }, () => {
                    this.updateTextureOverlay();
                });
            } else {
                delete availableDataLayerKey[data.name]
                this.setState({
                    availableDataLayerKey
                }, () => {
                    this.updateTextureOverlay();
                });
            }
        })
        socket.on('SEND_COUNTRY_SELECTED', data => {
            if (unSyncKey['Country']) 
                return;
            const {availableCountryFilterKey} = this.state;
            if (data && data.value) {
                availableCountryFilterKey[data.name] = data.value
                this.setState({
                    availableCountryFilterKey
                }, () => {
                    this.updateTextureOverlay();
                    this.updateBeacon();
                });
            } else {
                delete availableCountryFilterKey[data.name]
                this.setState({
                    availableCountryFilterKey
                }, () => {
                    this.updateTextureOverlay();
                    this.updateBeacon();
                });
            }
        })

        socket.on('rainfall_nrt', data => {
            const {availableDataLayerKey, dataLayerList} = this.state
            if (unSyncKey['Data Layer'] || !availableDataLayerKey['Rainfall Station']) 
                return;
            
            dataLayerList['Rainfall Station'] = data
            this.setState({dataLayerList})
        })
        socket.on('hydro_nrt', data => {
            const {availableDataLayerKey, dataLayerList} = this.state
            if (unSyncKey['Data Layer'] || !availableDataLayerKey['Hydro Meteorological']) 
                return;
            dataLayerList['Hydro Meteorological'] = data
            this.setState({dataLayerList})
        })
    }
    toggleImageOverlay = (data) => {
        const {overlay} = data
        const map = this.mapRef.current;
        if (map.hasLayer(overlay)) {
            overlay.removeFrom(map);
        } else {
            overlay.addTo(map);
        }
    }
    updateTextureOverlay = async() => {
        const {availableDataLayerKey, dateFiltered, availableCountryFilterKey} = this.state;
        let {overlays} = this.state;
        const res = await getTextureData();
        const map = this.mapRef.current;
        overlays.forEach(overlay => {
            overlay.removeFrom(map);
        });
        overlays = [];
        const data = {};
        let filtered = res.filter(element => availableDataLayerKey[element.name]);
        filtered = filtered.filter(element => availableCountryFilterKey[element.country]);
        filtered.forEach(item => {
            if (!data[item.name]) {
                data[item.name] = {};
            }
            if (!data[item.name][item.country]) {
                data[item.name][item.country] = item;
            } else {
                const temp = data[item.name][item.country];
                const tempDiff = Math.abs(dateFiltered.diff(temp.date));
                const diff = Math.abs(dateFiltered.diff(item.date));
                console.log(`tempDiff : ${tempDiff} diff:${diff}`)
                if (diff <= tempDiff) {
                    data[item.name][item.country] = item;
                }
            }

        });
        console.log('data', data);
        Object
            .keys(data)
            .forEach(key => {
                Object
                    .keys(data[key])
                    .forEach(keyCountry => {
                        const elem = data[key][keyCountry];
                        const {overlay} = elem;
                        overlay.data = elem;
                        overlay.addTo(map);
                        overlays.push(overlay);
                    });
            });

        this.setState({
            overlays
        }, () => {
            console.log('overlays', overlays)
        });
    }

    updateBeacon = async() => {
        const {availableBeaconLayerKey, dateFiltered, availableCountryFilterKey} = this.state;
        let {beaconLayerList} = this.state;
        Object
            .keys(availableBeaconLayerKey)
            .forEach(async key => {
                const mainType = key.split(' - ')[0]
                const subType = key.split(' - ')[1]
                const res = await getBeaconData(mainType, subType, dateFiltered);
                const filtered = res.filter(element => availableCountryFilterKey[element.country]);
                beaconLayerList[key] = filtered;
                console.log('beaconLayerList', beaconLayerList);

                this.setState({beaconLayerList});
            });
    }

    handleClickMarker = (event, id) => {
        const {lat, lng} = event.latlng
        console.log(`click id`, id);
        if (this.state.unSyncKey['Data Marker Click']) 
            return;
        socket.emit('ON_CLICK_MARKER', id)

        const center = {
            lat: lat,
            lng: lng
        }
        setTimeout(() => {
            this
                .mapRef
                .current
                .setView(center, this.state.zoom);
            if (this.state.unSyncKey['Location']) 
                return;
            socket.emit('MAP_CAMERA', center)
        }, 200);

    }
    onChangeSite = async(data) => {
        this.setState({currentSite: data})
        socket.emit('JOIN', data);

    }
    handleSlider = async(e) => {
        console.log(e.target.value);
        this.setState({
            currentDateRange: dateRangeDummy[e.target.value],
            currentDateRangeValue: e.target.value
        })
        if (this.state.unSyncKey['Date Slider']) 
            return;
        socket.emit('CHANGE_DATE_RANGE', e.target.value);
    }

    renderSiteSelection() {
        return (
            <DropdownButton
                style={{
                marginRight: -120,
                marginTop: 8
                
            }}
                className={'leaflet-control dropdown-subsubmenu'}
                size="sm"
                title={`site : ${this.state.currentSite}`}
                variant="light">
                {SITE.map(item => (
                    <Dropdown.Item
                        style={{zIndex: 2}}
                        onClick={() => {
                        this.onChangeSite(item.short)
                    }}>{item.title}</Dropdown.Item>
                ))}
            </DropdownButton>
        )
    }

    // renderBaseMapSelection() {     return (         <DropdownButton style={{
    //  marginRight: 16,                 marginTop: 8      }}
    // className={'leaflet-control'}             size="sm"       title="Basemap
    // Selection"             variant="secondary"> <Dropdown.Item key={'normal'}
    // value={normalUrl} onClick={() => { this.setState({baseMap:normalUrl})
    //   }}>{'Normal Basemap'}</Dropdown.Item> <Dropdown.Item key={'satellite'}
    // value={satelliteUrl} onClick={() => { this.setState({baseMap:normalUrl})
    //              }}>{'Satellite Basemap'}</Dropdown.Item> </DropdownButton>     )
    // }

    renderDataLayerPanel() {
        const {dataLayerList, availableDataLayerKey, availableLegendKey} = this.state
        return (
            <div className="leaflet-control-container">
                
                        <div>
                            {DATA_LAYER_LIST.map(item => {
                                return (
                                    <Checkbox
                                        className={'leaflet-control'}
                                        style={{
                                            borderRadius: 6,
                                            width: 175,
                                            marginLeft: 10
                                        }}
                                        checked={availableDataLayerKey[item.name]}
                                        onChange={async(v) => {
                                        if (v.target.checked) {
                                            availableDataLayerKey[item.name] = true;
                                            availableLegendKey[item.name] = true;
                                            this.setState({availableDataLayerKey, availableLegendKey});
                                            if (item.type === 'overlay') {
                                                this.toggleImageOverlay(item);
                                            } else {
                                                const data = await item.function ();
                                                dataLayerList[item.name] = data;
                                                this.setState({dataLayerList});
                                            }
                                            if (this.state.unSyncKey['Data Layer']) 
                                                return;
                                            socket.emit('SELECT_DATA_LAYER', {
                                                name: item.name,
                                                value: true
                                            })
                                        } else {
                                            if (item.type === 'overlay') {
                                                this.toggleImageOverlay(item);
                                            } else {
                                                delete dataLayerList[item.name];
                                                this.setState({dataLayerList})
                                            }
                                            delete availableDataLayerKey[item.name];
                                            delete availableLegendKey[item.name];
                                            this.setState({availableDataLayerKey, availableLegendKey});
                                            if (this.state.unSyncKey['Data Layer']) 
                                                return;
                                            socket.emit('SELECT_DATA_LAYER', {
                                                name: item.name,
                                                value: false
                                            })
                                        }
                                        console.log(v);
                                    }}>{item.name}</Checkbox>
                                )
                            })}
                        </div>
                    
            </div>
        )
    }
    renderBeaconPanel() {
        const {beaconLists, dateFiltered, availableBeaconLayerKey, availableLegendKey, beaconLayerList} = this.state
        return (
            <div className="leaflet-control-container">

                        <div>
                            {beaconLists.map(item => {
                                return (
                                    <Checkbox
                                        className={'leaflet-control'}
                                        style={{
                                            borderRadius: 6,
                                            width: 175,
                                            marginLeft: 10
                                        }}
                                        checked={availableBeaconLayerKey[item.name]}
                                        onChange={async(v) => {
                                        if (v.target.checked) {
                                            availableBeaconLayerKey[item.name] = true;
                                            availableLegendKey[item.name] = true;
                                            this.setState({
                                                availableBeaconLayerKey,
                                                availableLegendKey
                                            }, async() => {
                                                this.updateBeacon()
                                            });
                                            if (this.state.unSyncKey['Data Layer']) 
                                                return;
                                            socket.emit('BEACON_SELECTED', {
                                                name: item.name,
                                                value: true
                                            })
                                        } else {
                                            delete availableBeaconLayerKey[item.name];
                                            delete availableLegendKey[item.name];
                                            delete beaconLayerList[item.name];
                                            this.setState({
                                                availableBeaconLayerKey,
                                                availableLegendKey,
                                                beaconLayerList
                                            }, async() => {/*this.updateBeacon();*/
                                            });
                                            if (this.state.unSyncKey['Data Layer']) 
                                                return;
                                            socket.emit('BEACON_SELECTED', {
                                                name: item.name,
                                                value: false
                                            })
                                        }
                                    }}>{item.name}
                                    </Checkbox>
                                )
                            })}
                        </div>
            </div>
        )
    }

    renderTexturePanel() {
        const {
            textureList,
            availableDataLayerKey,
            availableLegendKey,
            dateFiltered,
            availableCountryFilterKey,
            textureLayers
        } = this.state
        return (
            <div className="leaflet-control-container">
                
                        <div>
                            
                            {textureList.map(item => {
                                return (
                                    <Checkbox
                                        className={'leaflet-control'}
                                        style={{
                                            borderRadius: 6,
                                            width: 175,
                                            marginLeft: 10
                                        }}
                                        checked={availableDataLayerKey[item.name]}
                                        onChange={async(v) => {
                                        if (v.target.checked) {
                                            availableDataLayerKey[item.name] = true;
                                            availableLegendKey[item.name] = true;
                                            this.setState({
                                                availableDataLayerKey,
                                                availableLegendKey
                                            }, () => {
                                                this.updateTextureOverlay();
                                            });
                                            if (this.state.unSyncKey['Texture Layer']) 
                                                return;
                                            socket.emit('TEXTURE_SELECTED', {
                                                name: item.name,
                                                value: true
                                            });
                                        } else {
                                            delete availableDataLayerKey[item.name];
                                            delete availableLegendKey[item.name];
                                            this.setState({
                                                availableDataLayerKey,
                                                availableLegendKey
                                            }, () => {
                                                this.updateTextureOverlay();
                                            });
                                            if (this.state.unSyncKey['Texture Layer']) 
                                                return;
                                            socket.emit('TEXTURE_SELECTED', {
                                                name: item.name,
                                                value: false
                                            });
                                        }
                                        console.log(v);
                                    }}>{item.subType}</Checkbox>
                                )
                            })}
                            
                        </div>

            </div>
        )
    }

    renderCountryFilterPanel() {
        const {dataLayerList, availableCountryFilterKey} = this.state
        return (
            <div className="leaflet-control-container">
                        <div>
                        
                            {AREA_FILTER.map(item => {
                                return (
                                    <Checkbox
                                        className={'leaflet-control'}
                                        style={{
                                            borderRadius: 6,
                                            width: 175,
                                            marginLeft: 10
                                        }}
                                        checked={availableCountryFilterKey[item]}
                                        onChange={async(v) => {
                                        if (v.target.checked) {
                                            availableCountryFilterKey[item] = true;
                                            this.setState({
                                                availableCountryFilterKey
                                            }, () => {
                                                this.updateTextureOverlay();
                                                this.updateBeacon();
                                            });
                                            if (this.state.unSyncKey['Country']) 
                                                return;
                                            socket.emit('COUNTRY_SELECTED', {
                                                name: item,
                                                value: true
                                            });
                                        } else {
                                            delete availableCountryFilterKey[item];
                                            this.setState({
                                                availableCountryFilterKey
                                            }, () => {
                                                this.updateTextureOverlay();
                                                this.updateBeacon();
                                            });
                                            if (this.state.unSyncKey['Country']) 
                                                return;
                                            socket.emit('COUNTRY_SELECTED', {
                                                name: item,
                                                value: false
                                            });
                                        }
                                        console.log(v);
                                    }}>{item}</Checkbox>
                                )
                            })}
                        </div>
            </div>
        )
    }

    renderUnSyncPanel() {
        const {unSyncKey} = this.state;
        return (
            <div className="leaflet-control-container">
                <div>
                            {EVENT_LIST.map(item => {
                                return (
                                    <Checkbox
                                        className={'leaflet-control'}
                                        style={{
                                            borderRadius: 6,
                                            width: 150,
                                            marginLeft: 10
                                        }}
                                        checked={!unSyncKey[item]}
                                        onChange={async(v) => {
                                        if (!v.target.checked) {
                                            unSyncKey[item] = true;
                                        } else {
                                            delete unSyncKey[item];
                                        }
                                        this.setState({unSyncKey});
                                    }}>{item}</Checkbox>
                                )
                            })}
                        
                  </div>
                  </div>
        )
    }

    renderBottomDrawer() {
        const {isShowBottomDrawer, dateFiltered} = this.state
        let year_marks = {}
        for (let index = 2000; index <= 2022; index++) {
            year_marks[index] = index
        }
        let month_marks = {}
        for (let index = 1; index <= 12; index++) {
            month_marks[index] = moment(`2022-${index}-1`).format('MMM')
        }
        let day_marks = {}
        for (let index = 1; index <= dateFiltered.daysInMonth(); index++) {
            day_marks[index] = index
        }

        return (
            <div className="leaflet-control-container">
                <div
                    className={'leaflet-control'}
                    style={{
                    height: 'auto',
                    width: '50%',
                    backgroundColor: '#0000',
                    left: '50%',
                    bottom: 0,
                    position: 'absolute',
                    transform: 'translate(-50%, 0%)',
                    margin: 0
                }}>
                    <Drawer
                        title={`Select Data Range: ${dateFiltered.format('YYYY-MM-DD')}`}
                        height={280}
                        headerStyle={{
                        paddingBottom: 8
                    }}
                        maskStyle={{
                        backgroundColor: '#fff0'
                    }}
                        placement={'bottom'}
                        closable={true}
                        getContainer={false}
                        onClose={() => {
                        this.setState({isShowBottomDrawer: false});
                        this
                            .mapRef
                            .current
                            .dragging
                            .enable();
                    }}
                        visible={isShowBottomDrawer}>

                        <div
                            style={{
                            marginTop: -20
                        }}>{`Year: `}</div>
                        <Slider
                            marks={year_marks}
                            min={2000}
                            max={2022}
                            included={false}
                            value={dateFiltered.year()}
                            onChange={(value) => {
                            const currentDate = `${value}-${dateFiltered.month() + 1}-${dateFiltered.date()}`;
                            this.setState({
                                dateFiltered: moment(currentDate)
                            }, async() => {
                                this.updateTextureOverlay();
                                this.updateBeacon();
                            });
                            socket.emit('CHANGE_DATE_RANGE', currentDate);
                            socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                xrange: [
                                    moment(currentDate)
                                        .add(-30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss'),
                                    moment(currentDate)
                                        .add(30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss')
                                ]
                            })
                        }}/>
                        <div>{`Month: `}</div>
                        <Slider
                            marks={month_marks}
                            min={1}
                            max={12}
                            included={false}
                            value={dateFiltered.month() + 1}
                            onChange={(value) => {
                            const currentDate = `${dateFiltered.year()}-${value}-${dateFiltered.date()}`;
                            this.setState({
                                dateFiltered: moment(currentDate)
                            }, async() => {
                                this.updateBeacon();
                            });
                            socket.emit('CHANGE_DATE_RANGE', currentDate);
                            socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                xrange: [
                                    moment(currentDate)
                                        .add(-30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss'),
                                    moment(currentDate)
                                        .add(30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss')
                                ]
                            })
                        }}/>
                        <div>{`Day: `}</div>
                        <Slider
                            marks={day_marks}
                            min={1}
                            max={dateFiltered.daysInMonth()}
                            included={false}
                            value={dateFiltered.date()}
                            onChange={(value) => {
                            const currentDate = `${dateFiltered.year()}-${dateFiltered.month() + 1}-${value}`;
                            this.setState({
                                dateFiltered: moment(currentDate)
                            }, async() => {
                                this.updateBeacon();
                            });
                            socket.emit('CHANGE_DATE_RANGE', currentDate);
                            socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                xrange: [
                                    moment(currentDate)
                                        .add(-30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss'),
                                    moment(currentDate)
                                        .add(30, 'd')
                                        .format('YYYY-MM-DD HH:mm:ss')
                                ]
                            })
                        }}/>
                    </Drawer>
                </div>
            </div>
        )
    }

    onDragStart = () => {
        this.setState({
            activeDrags: ++this.state.activeDrags
        });
        this
            .mapRef
            .current
            .dragging
            .disable();
    };

    onDragStop = () => {
        this.setState({
            activeDrags: --this.state.activeDrags
        });
        this
            .mapRef
            .current
            .dragging
            .enable();
    };
    renderLegend(name, legendUrl) {
        const dragHandlers = {
            onStart: this.onDragStart,
            onStop: this.onDragStop
        };
        const {
            availableDataLayerKey,
            availableLegendKey,
            hideLegendKey,
            overlays,
            beaconLayerList,
            availableBeaconLayerKey
        } = this.state

        return (
            <div className={'leaflet-control-container'}>
                <div className={'leaflet-control'}>
                    {DATA_LAYER_LIST.map(item => {
                        return availableDataLayerKey[item.name] && availableLegendKey[item.name]
                            ? (<LegendItem
                                onClose={() => {
                                delete availableLegendKey[item.name];
                                this.setState({availableLegendKey})
                            }}
                                onHide={() => {
                                if (hideLegendKey[item.name]) {
                                    delete hideLegendKey[item.name];
                                } else {
                                    hideLegendKey[item.name] = true;
                                }
                                this.setState({hideLegendKey});
                            }}
                                isHide={hideLegendKey[item.name]}
                                dragHandlers={dragHandlers}
                                legendUrl={item.legendUrl
                                ? item.legendUrl.default
                                : ''}
                                name={item.name}/>)
                            : null
                    })}
                    {overlays.map(elem => {
                        const item = elem.data
                        return availableDataLayerKey[item.name] && availableLegendKey[item.name]
                            ? (<LegendItem
                                onClose={() => {
                                delete availableLegendKey[item.name];
                                this.setState({availableLegendKey})
                            }}
                                onHide={() => {
                                if (hideLegendKey[item.name]) {
                                    delete hideLegendKey[item.name];
                                } else {
                                    hideLegendKey[item.name] = true;
                                }
                                this.setState({hideLegendKey});
                            }}
                                isHide={hideLegendKey[item.name]}
                                dragHandlers={dragHandlers}
                                legendUrl={item.legendUrl
                                ? item.legendUrl
                                : ''}
                                name={`${item.name}-${item.country}`}/>)
                            : null
                    })
}
                    {Object
                        .keys(beaconLayerList)
                        .map((key) => {
                            const subType = key.split(' - ')[1]
                            return availableBeaconLayerKey[key] && availableLegendKey[key]
                                ? (<LegendItem
                                    onClose={() => {
                                    delete availableLegendKey[key];
                                    this.setState({availableLegendKey})
                                }}
                                    onHide={() => {
                                    if (hideLegendKey[key]) {
                                        delete hideLegendKey[key];
                                    } else {
                                        hideLegendKey[key] = true;
                                    }
                                    this.setState({hideLegendKey});
                                }}
                                    isHide={hideLegendKey[key]}
                                    dragHandlers={dragHandlers}
                                    legendUrl={legends_beacon[subType] || ''}
                                    name={`${key}`}/>)
                                : null
                        })
}
                </div>
            </div>
        )
    }

    render() {
        const position = [this.state.lat, this.state.lng]
        const {beaconLayerList, dataLayerList, beaconLists} = this.state
        console.log('dataLayerList', dataLayerList)
        return (
        <div>
            <div className="App">
     <Navbar bg="myGrey" variant="dark" expand="lg">
  <Container fluid>
    <Navbar.Brand style={{marginLeft: 20}}>
    <img
        src={logo}
        width="120"
        height="50"
        alt="logo..."
      /></Navbar.Brand>
    <Navbar.Toggle aria-controls="navbarScroll" />
    <Navbar.Collapse id="navbarScroll">
      <Nav
        className="ml-auto my-2 my-lg-0"
        style={{ maxHeight: '100px',
                 marginRight: 145,
                  }}
        navbarScroll
      >
          <Nav.Link onClick={() => {
                                    this.setState({
                                        isShowBottomDrawer: !this.state.isShowBottomDrawer
                                    });
                                    this
                                        .mapRef
                                        .current
                                        .dragging
                                        .disable()
                                }}><img
                                src="https://cdn-icons-png.flaticon.com/512/55/55238.png?w=740&t=st=1648626563~exp=1648627163~hmac=3a514d490e7a5788fd6949eb7a596763a2460f62d29bcc18d8f1a115deb1bea8"
                                width="30"
                                height="30"
                                alt=""
                              /></Nav.Link>
        <NavDropdownMenu title={<img
                                src="https://sv1.picz.in.th/images/2022/04/17/86i9EI.png"
                                width="30"
                                height="30"
                                alt=""
                              />} id="navbarScrollingDropdown"
                              className='dropdown-sub3menu'>
        
          <DropdownSubmenu title="Show Data Layer Panel" className='dropdown-sub5menu'>
                {this.renderDataLayerPanel()}
          </DropdownSubmenu>
          <DropdownSubmenu title="Show Beacon Panel" className='dropdown-sub5menu'>
                {this.renderBeaconPanel()}
          </DropdownSubmenu>
          <DropdownSubmenu title="Show Texture Panel" className='dropdown-sub5menu'>
                {this.renderTexturePanel()}
          </DropdownSubmenu>
          <NavDropdown.Divider />
          <DropdownSubmenu title="Show Country Filter Panel" className='dropdown-sub5menu'>
                {this.renderCountryFilterPanel()}
          </DropdownSubmenu>
        </NavDropdownMenu>
        <NavDropdownMenu style={{marginTop: 13, marginRight: 500}} title={<img
                                src="https://sv1.picz.in.th/images/2022/04/25/8LftiP.png"
                                width="30"
                                height="30"
                                alt=""
                              />} id="navbarScrollingDropdown"
                              className='dropdown-sub4menu'>
        
          <DropdownSubmenu title="Sync/Un-sync" className='dropdown-sub6menu'>
                                           <div
                                                className='col-centered'
                                                style={{
                                                    textAlign: 'center',
                                                    borderRadius: 6,
                                                    width: 160
                                                }}
                                                variant="secondary"
                                                size="sm"
                                                onClick={() => {
                                                const s = !this.state.isSync;
                                                this.setState({isSync: s});
                                                if (this.state.unSyncKey['Sync State']) 
                                                    return;
                                                socket.emit('SYNC', s);
                                            }}>
                                                {this.state.isSync
                                                    ? 'Un-sync'   
                                                    : 'Sync'}
                                            </div>
          </DropdownSubmenu>
          <DropdownSubmenu title="Show Sync Profile Panel" className='dropdown-sub6menu'>
                {this.renderUnSyncPanel()}
          </DropdownSubmenu>
          </NavDropdownMenu>
          {this.renderSiteSelection()}
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>
    </div>
            <div id="MAIN">
                <MapContainer
                
                    whenCreated={mapInstance => {
                    this.mapRef.current = mapInstance;
                    this
                        .mapRef
                        .current
                        .on('drag', (e) => {
                            if (this.state.unSyncKey['Location']) 
                                return;
                            socket.emit('MAP_CAMERA', e.target.getCenter());
                        });
                    this
                        .mapRef
                        .current
                        .on('zoomend', (e) => {
                            console.log('e', e);
                            if (this.state.unSyncKey['Location']) 
                                return;
                            socket.emit('MAP_ZOOM', {
                                center: e
                                    .target
                                    .getCenter(),
                                zoom: e
                                    .target
                                    .getZoom()
                            });
                        })
                }}
                    center={position}
                    scrollWheelZoom={true}
                    zoom={this.state.zoom}
                    style={{
                    height: '92.3vh'
                }}>
                    <LayersControl position="bottomright">
                        <LayersControl.BaseLayer checked name="OpenStreetMap">
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url={normalUrl}/>
                        </LayersControl.BaseLayer>
                        <LayersControl.BaseLayer name="ErsiWorldImagery">
                            <TileLayer
                                attribution='&copy; <a href="https://www.esri.com/">Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community</a> contributors'
                                url={satelliteUrl}/>
                        </LayersControl.BaseLayer>
                    </LayersControl>
                    {Object
                        .keys(dataLayerList)
                        .map(layer => {
                            console.log('layer', layer);
                            const layerItem = DATA_LAYER_LIST.find(obj => obj.name === layer);
                            return dataLayerList[layer].map(item => {
                                this.markerRefs[item.id] = React.createRef();
                                return (<DataMarker
                                    markerRef={(ref) => {
                                    this.markerRefs[item.id] = ref
                                }}
                                    position={item.position}
                                    id={item.id}
                                    icon={layerItem.icon}
                                    isUnSync={this.state.unSyncKey['Data Marker Click']}
                                    data={item}
                                    detailKeys={layerItem.detailKeys}
                                    vizPath={layerItem.path}
                                    emitter={() => {
                                    socket.emit('OPEN_DATA_DETAIL', item.id);
                                }}
                                    onClick={{
                                    click: (event) => this.handleClickMarker(event, item.id)
                                }}/>)
                            })
                        })}
                    {Object
                        .keys(beaconLayerList)
                        .map(layer => {
                            console.log('beacon layer', layer);
                            return beaconLayerList[layer].map(item => {
                                console.log('render item', item);
                                this.markerRefs[item.id] = React.createRef();
                                return (<BeaconMarker
                                    markerRef={(ref) => {
                                    this.markerRefs[item.id] = ref
                                }}
                                    position={item.position}
                                    id={item.id}
                                    name={item.name}
                                    timeSeriesData={item.timeSeriesData}
                                    isUnSync={this.state.unSyncKey['Data Marker Click']}
                                    data={item.data}
                                    detailKeys={item.detailKeys}
                                    icon={item.icon}
                                    vizPath={item.path}
                                    emitter={() => {
                                    socket.emit('OPEN_DATA_DETAIL', item.id);
                                }}
                                    onClick={{
                                    click: (event) => this.handleClickMarker(event, item.id)
                                }}/>)
                            })
                        })}

                    <div className="leaflet-control-container">
                        <div className="leaflet-top leaflet-left">
                                <div>
                                <Button
                                    className={'leaflet-control'}
                                    variant="secondary"
                                    size="sm"
                                    style={{
                                    height: 32,
                                    marginTop: 83,
                                    marginRight: 16,
                                    zIndex: 1
                                }}
                                    onClick={() => {
                                    const elem = document.getElementById('MAIN');
                                    if (this.state.isFullScreen) {
                                        closeFullscreen();
                                        this.setState({isFullScreen: false});
                                    } else {
                                        openFullscreen(elem);
                                        this.setState({isFullScreen: true});
                                    }
                                }}>
                                    {this.state.isFullScreen
                                        ? <img
                                            src="https://sv1.picz.in.th/images/2022/04/17/8600Hg.png"
                                            width="18"
                                            height="18"
                                            alt=""
                                          />
                                        : <img
                                        src="https://sv1.picz.in.th/images/2022/04/17/860WfS.png"
                                        width="18"
                                        height="18"
                                        alt=""/>}
                                </Button>
                                </div>
                        </div>
                    </div>
                  
                    {this.renderBottomDrawer()}
                    {this.renderLegend()}
                </MapContainer>
            </div>
            </div>
        )
    }
}

const LegendItem = ({
    name,
    legendUrl,
    dragHandlers,
    onClose,
    onHide,
    isHide
}) => {
    const random = Math.floor(Math.random() * 60)

    return (
        <Draggable
            grid={[10, 10]}
            handle="#dragBar"
            {...dragHandlers}
            defaultPosition={{
            x: 25 + random,
            y: 400 + random
        }}>
            <div
                className="leaflet-control"
                style={{
                backgroundColor: 'white',
                position: 'absolute',
                boxShadow: '2px 6px 8px 1px #0003',
                width: isHide
                    ? 100
                    : 240,
                height: isHide
                    ? 25
                    : 280
            }}>
                <div
                    id="dragBar"
                    onClick={() => {
                    console.log('dd')
                }}
                    style={{
                    height: 25,
                    width: '100%',
                    backgroundColor: '#F0F0F0',
                    cursor: 'move',
                    overflowWrap: 'normal',
                    zIndex: 10000 + random
                }}>
                    <Row style={{
                        marginLeft: 0
                    }}>
                        <div
                            onClick={onClose}
                            style={{
                            paddingTop: 4,
                            height: 25,
                            width: 25,
                            backgroundColor: '#EF4B4B',
                            textAlign: 'center',
                            cursor: 'pointer',
                            color: '#fff',
                            fontSize: '30',
                            fontWeight: 'bold'
                        }}>X</div>
                        <div
                            onClick={onHide}
                            style={{
                            paddingTop: 4,
                            height: 25,
                            width: 25,
                            backgroundColor: '#F9B208',
                            textAlign: 'center',
                            cursor: 'pointer',
                            color: '#fff',
                            fontSize: '30',
                            fontWeight: 'bold'
                        }}>-</div>
                        <div
                            style={{
                            textAlign: 'center',
                            fontWeight: 'bold',
                            marginTop: 4,
                            marginLeft: 8
                        }}>{name}</div>
                    </Row>
                    {!isHide
                        ? (
                            <div
                                style={{
                                justifyContent: 'center',
                                textAlign: 'center',
                                padding: 8
                            }}>
                                <img
                                    src={legendUrl}
                                    style={{
                                    width: 220,
                                    height: 220,
                                    objectFit: 'contain'
                                }}/>
                            </div>
                        )
                        : null}
                </div>
            </div>
        </Draggable>
    )
}

const rootElement = document.getElementById("root");
