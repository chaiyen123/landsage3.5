import json
import csv
import pandas as pd
import numpy as np
import glob
import datetime as dt
from datetime import datetime,  timedelta
from datetime import date

path = "D:/Landsage/Scrap/"
path_con = "D:/Landsage/pinmanagerserver-master/data/con_file/"
path_final = "D:/Landsage/pinmanagerserver-master/data/csv/"
all_files = glob.glob(path+"hydro_nrt.json")
yesterday = datetime.now() - timedelta(1)
file_select = datetime.strftime(yesterday, '%Y-%m-%d')

# path = "C:/Users/witch/Progarming/data/"
# path_con = "C:/Users/witch/Progarming/con_file/"
# path_final = "C:/Users/witch/Progarming/data2/"
# file_select = '2022-05-15'
# file_select = '2022-01-01'
all_files = glob.glob(path+'*'+file_select+"*.json")

print (all_files)
for f in all_files: 
    file = str(f)
    file = file.split('\\')
    file = file[-1]
    with open(path+file) as json_file:
        jsondata = json.load(json_file)
    file = file.split('.')
    file = file[0]
    data_file = open(path_con+file+'.csv', 'w', newline='')
    csv_writer = csv.writer(data_file)

    count = 0
    for data in jsondata:
        if count == 0:
            header = data.keys()
            csv_writer.writerow(header)
            count += 1
        csv_writer.writerow(data.values())

    data_file.close()




all_files = glob.glob(path_con +file_select+ "*.csv")
print (all_files)
li_3 = [pd.read_csv(filename, index_col = None, header = 0) for filename in all_files]
df_Rainfall = pd.concat (li_3, axis=0,ignore_index = True)
df_WaterLevel = pd.concat (li_3, axis=0,ignore_index = True)

df_Rainfall.drop ([ 'name', 'shortName', 'latitude', 'longitude', 'river',
       'country', 'floodStage', 'alarmStage','waterLevel',
       'rainFall1H', 'rainFall6H', 'rainFall12H', 'rainFall24H',
       'meanSeaLevel', 'wlSensorType', 'stationType',
       'wlSensor', 'rainfallSensor', 'tempSensor', 'batterySensor',
       'parsingEnabled', 'showOnWebsite', 'sortOrder', 'successRate',
       'lastStatus', 'notes', 'notice', 'timezone','rainFall7to7','wqSensors'],inplace = True , axis = 1)
df_WaterLevel.drop ([ 'name', 'shortName', 'latitude', 'longitude', 'river',
       'country', 'floodStage', 'alarmStage','rainFall7to7',
       'rainFall1H', 'rainFall6H', 'rainFall12H', 'rainFall24H',
       'meanSeaLevel', 'wlSensorType', 'stationType',
       'wlSensor', 'rainfallSensor', 'tempSensor', 'batterySensor',
       'parsingEnabled', 'showOnWebsite', 'sortOrder', 'successRate',
       'lastStatus', 'notes', 'notice', 'timezone','rainFall7to7','rainFall','wqSensors'],inplace = True , axis = 1)

df_Rainfall['lastMeasurement'] = pd.to_datetime(df_Rainfall['lastMeasurement']).dt.strftime('%Y-%m-%d')
df_WaterLevel['lastMeasurement'] = pd.to_datetime(df_WaterLevel['lastMeasurement']).dt.strftime('%Y-%m-%d')

first_column_1 = df_Rainfall['lastMeasurement']
df_Rainfall.insert(0, 'Date', first_column_1)
first_column_2 = df_WaterLevel['lastMeasurement']
df_WaterLevel.insert(0, 'Date', first_column_2)
df_Rainfall.drop ([ 'lastMeasurement'],inplace = True , axis = 1)
df_WaterLevel.drop ([ 'lastMeasurement'],inplace = True , axis = 1)
df_Rainfall['Interpolated_Rainfall'] = df_Rainfall ['rainFall']

df_Rainfall.to_csv("Rainfall.csv",index = False,date_format='%Y-%m-%d')
df_WaterLevel.to_csv("WaterLevel.csv",index = False, date_format='%Y-%m-%d')




df_Rainfall_TanChau = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_TanChau = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_Chaktomuk = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_Chaktomuk = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_KhongChiam = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_KhongChiam = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_Vientien = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_Vientien = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)



df_Rainfall_Thoeng = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_Thoeng = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_LuangPrabang = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_LuangPrabang = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_Kontum = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_Kontum = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)
df_Rainfall_KampongAmpil = pd.read_csv ("Rainfall.csv", index_col= None , header = 0)
df_WaterLevel_KampongAmpil = pd.read_csv ("WaterLevel.csv", index_col= None , header = 0)




df_Rainfall_Thoeng.drop(df_Rainfall_Thoeng[(df_Rainfall_Thoeng['stationId'] !=70103)].index, inplace=True)
df_Rainfall_LuangPrabang.drop(df_Rainfall_LuangPrabang[(df_Rainfall_LuangPrabang['stationId'] !=11201)].index, inplace=True)
df_Rainfall_Kontum.drop(df_Rainfall_Kontum[(df_Rainfall_Kontum['stationId'] !=440201)].index, inplace=True)
df_Rainfall_KampongAmpil.drop(df_Rainfall_KampongAmpil[(df_Rainfall_KampongAmpil['stationId'] !=19905)].index, inplace=True)
df_Rainfall_Thoeng.drop(df_Rainfall_Thoeng[(df_Rainfall_Thoeng['Date'] !=file_select)].index, inplace=True)
df_Rainfall_LuangPrabang.drop(df_Rainfall_LuangPrabang[(df_Rainfall_LuangPrabang['Date'] !=file_select)].index, inplace=True)
df_Rainfall_Kontum.drop(df_Rainfall_Kontum[(df_Rainfall_Kontum['Date'] !=file_select)].index, inplace=True)
df_Rainfall_KampongAmpil.drop(df_Rainfall_KampongAmpil[(df_Rainfall_KampongAmpil['Date'] !=file_select)].index, inplace=True)
df_WaterLevel_Thoeng.drop(df_WaterLevel_Thoeng[(df_WaterLevel_Thoeng['stationId'] !=70103)].index, inplace=True)
df_WaterLevel_LuangPrabang.drop(df_WaterLevel_LuangPrabang[(df_WaterLevel_LuangPrabang['stationId'] !=11201)].index, inplace=True)
df_WaterLevel_Kontum.drop(df_WaterLevel_Kontum[(df_WaterLevel_Kontum['stationId'] !=440201)].index, inplace=True)
df_WaterLevel_KampongAmpil.drop(df_WaterLevel_KampongAmpil[(df_WaterLevel_KampongAmpil['stationId'] !=19905)].index, inplace=True)
df_Rainfall_Thoeng2= df_Rainfall_Thoeng["rainFall"].mean()
df_Rainfall_LuangPrabang2= df_Rainfall_LuangPrabang["rainFall"].mean()
df_Rainfall_Kontum2= df_Rainfall_Kontum["rainFall"].mean()
df_Rainfall_KampongAmpil2= df_Rainfall_KampongAmpil["rainFall"].mean()
df_WaterLevel_Thoeng2= df_WaterLevel_Thoeng["waterLevel"].mean()
df_WaterLevel_LuangPrabang2= df_WaterLevel_LuangPrabang["waterLevel"].mean()
df_WaterLevel_Kontum2= df_WaterLevel_Kontum["waterLevel"].mean()
df_WaterLevel_KampongAmpil2= df_WaterLevel_KampongAmpil["waterLevel"].mean()
df_Rainfall_Thoeng["rainFall"] = df_Rainfall_Thoeng2
df_Rainfall_LuangPrabang["rainFall"] =df_Rainfall_LuangPrabang2
df_Rainfall_Kontum["rainFall"] = df_Rainfall_Kontum2
df_Rainfall_KampongAmpil["rainFall"] = df_Rainfall_KampongAmpil2
df_WaterLevel_Thoeng["waterLevel"] = df_WaterLevel_Thoeng2
df_WaterLevel_LuangPrabang["waterLevel"] = df_WaterLevel_LuangPrabang2
df_WaterLevel_Kontum["waterLevel"] =df_WaterLevel_Kontum2
df_WaterLevel_KampongAmpil["waterLevel"] =df_WaterLevel_KampongAmpil2
df_Rainfall_Thoeng =  df_Rainfall_Thoeng.drop_duplicates()
df_Rainfall_LuangPrabang =  df_Rainfall_LuangPrabang.drop_duplicates()
df_Rainfall_Kontum =  df_Rainfall_Kontum.drop_duplicates()
df_Rainfall_KampongAmpil =  df_Rainfall_KampongAmpil.drop_duplicates()
df_WaterLevel_Thoeng =  df_WaterLevel_Thoeng.drop_duplicates()
df_WaterLevel_LuangPrabang =  df_WaterLevel_LuangPrabang.drop_duplicates()
df_WaterLevel_Kontum =  df_WaterLevel_Kontum.drop_duplicates()
df_WaterLevel_KampongAmpil =  df_WaterLevel_KampongAmpil.drop_duplicates()



df_Rainfall_TanChau.drop(df_Rainfall_TanChau[(df_Rainfall_TanChau['stationId'] !=19803)].index, inplace=True)
df_Rainfall_Chaktomuk.drop(df_Rainfall_Chaktomuk[(df_Rainfall_Chaktomuk['stationId'] !=33401)].index, inplace=True)
df_Rainfall_KhongChiam.drop(df_Rainfall_KhongChiam[(df_Rainfall_KhongChiam['stationId'] !=13801)].index, inplace=True)
df_Rainfall_Vientien.drop(df_Rainfall_Vientien[(df_Rainfall_Vientien['stationId'] !=11901)].index, inplace=True)
df_Rainfall_TanChau.drop(df_Rainfall_TanChau[(df_Rainfall_TanChau['Date'] !=file_select)].index, inplace=True)
df_Rainfall_Chaktomuk.drop(df_Rainfall_Chaktomuk[(df_Rainfall_Chaktomuk['Date'] !=file_select)].index, inplace=True)
df_Rainfall_KhongChiam.drop(df_Rainfall_KhongChiam[(df_Rainfall_KhongChiam['Date'] !=file_select)].index, inplace=True)
df_Rainfall_Vientien.drop(df_Rainfall_Vientien[(df_Rainfall_Vientien['Date'] !=file_select)].index, inplace=True)
df_WaterLevel_TanChau.drop(df_WaterLevel_TanChau[(df_WaterLevel_TanChau['stationId'] !=19803)].index, inplace=True)
df_WaterLevel_Chaktomuk.drop(df_WaterLevel_Chaktomuk[(df_WaterLevel_Chaktomuk['stationId'] !=33401)].index, inplace=True)
df_WaterLevel_KhongChiam.drop(df_WaterLevel_KhongChiam[(df_WaterLevel_KhongChiam['stationId'] !=13801)].index, inplace=True)
df_WaterLevel_Vientien.drop(df_WaterLevel_Vientien[(df_WaterLevel_Vientien['stationId'] !=11901)].index, inplace=True)
df_Rainfall_TanChau2= df_Rainfall_TanChau["rainFall"].mean()
df_Rainfall_Chaktomuk2= df_Rainfall_Chaktomuk["rainFall"].mean()
df_Rainfall_KhongChiam2= df_Rainfall_KhongChiam["rainFall"].mean()
df_Rainfall_Vientien2= df_Rainfall_Vientien["rainFall"].mean()
df_WaterLevel_TanChau2= df_WaterLevel_TanChau["waterLevel"].mean()
df_WaterLevel_Chaktomuk2= df_WaterLevel_Chaktomuk["waterLevel"].mean()
df_WaterLevel_KhongChiam2= df_WaterLevel_KhongChiam["waterLevel"].mean()
df_WaterLevel_Vientien2= df_WaterLevel_Vientien["waterLevel"].mean()
df_Rainfall_TanChau["rainFall"] = df_Rainfall_TanChau2
df_Rainfall_Chaktomuk["rainFall"] =df_Rainfall_Chaktomuk2
df_Rainfall_KhongChiam["rainFall"] = df_Rainfall_KhongChiam2
df_Rainfall_Vientien["rainFall"] = df_Rainfall_Vientien2
df_WaterLevel_TanChau["waterLevel"] = df_WaterLevel_TanChau2
df_WaterLevel_Chaktomuk["waterLevel"] = df_WaterLevel_Chaktomuk2
df_WaterLevel_KhongChiam["waterLevel"] =df_WaterLevel_KhongChiam2
df_WaterLevel_Vientien["waterLevel"] =df_WaterLevel_Vientien2
df_Rainfall_TanChau =  df_Rainfall_TanChau.drop_duplicates()
df_Rainfall_Chaktomuk =  df_Rainfall_Chaktomuk.drop_duplicates()
df_Rainfall_KhongChiam =  df_Rainfall_KhongChiam.drop_duplicates()
df_Rainfall_Vientien =  df_Rainfall_Vientien.drop_duplicates()
df_WaterLevel_TanChau =  df_WaterLevel_TanChau.drop_duplicates()
df_WaterLevel_Chaktomuk =  df_WaterLevel_Chaktomuk.drop_duplicates()
df_WaterLevel_KhongChiam =  df_WaterLevel_KhongChiam.drop_duplicates()
df_WaterLevel_Vientien =  df_WaterLevel_Vientien.drop_duplicates()



df_Rainfall_TanChau.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_TanChau.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_Rainfall_Chaktomuk.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_Chaktomuk.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_Rainfall_KhongChiam.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_Vientien.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_TanChau.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_Chaktomuk.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_KhongChiam.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_Vientien.drop(['stationId'] , inplace=True, axis = 1)




df_Rainfall_Thoeng.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_LuangPrabang.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_Kontum.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_KampongAmpil.drop(['stationId'] , inplace=True, axis = 1)
df_Rainfall_Thoeng.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_Rainfall_LuangPrabang.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_Rainfall_Kontum.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_Rainfall_KampongAmpil.drop(['Interpolated_Rainfall'] , inplace=True, axis = 1)
df_WaterLevel_Thoeng.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_LuangPrabang.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_Kontum.drop(['stationId'] , inplace=True, axis = 1)
df_WaterLevel_KampongAmpil.drop(['stationId'] , inplace=True, axis = 1)




df_Rainfall_TanChau.columns = ['Date','Rainfall'] 
df_Rainfall_Chaktomuk.columns = ['Date','Rainfall'] 
df_Rainfall_KhongChiam.columns = ['Date','Rainfall','Interpolated_Rainfall']
df_Rainfall_Vientien.columns = ['Date','Rainfall','Interpolated_Rainfall'] 
df_WaterLevel_TanChau.columns = ['Date','WaterLevel'] 
df_WaterLevel_Chaktomuk.columns = ['Date','WaterLevel'] 
df_WaterLevel_KhongChiam.columns = ['Date','WaterLevel'] 
df_WaterLevel_Vientien.columns = ['Date','WaterLevel'] 




df_Rainfall_Thoeng.columns = ['Date','Rainfall'] 
df_Rainfall_LuangPrabang.columns = ['Date','Rainfall'] 
df_Rainfall_Kontum.columns = ['Date','Rainfall']
df_Rainfall_KampongAmpil.columns = ['Date','Rainfall'] 
df_WaterLevel_Thoeng.columns = ['Date','WaterLevel'] 
df_WaterLevel_LuangPrabang.columns = ['Date','WaterLevel'] 
df_WaterLevel_Kontum.columns = ['Date','WaterLevel'] 
df_WaterLevel_KampongAmpil.columns = ['Date','WaterLevel'] 




df_Rainfall_TanChau.to_csv(path_final+r"Beacon_10.8006_105.2480_Rainfall_TimeSeries_TanChau.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_Chaktomuk.to_csv(path_final+r"Beacon_11.5629_104.9352_Rainfall_TimeSeries_Chaktomuk.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_KhongChiam.to_csv(path_final+r"Beacon_15.3220_105.4934_Rainfall_TimeSeries_KhongChiam.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_Vientien.to_csv(path_final+r"Beacon_17.9309_102.6155_Rainfall_TimeSeries_Vientien.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_TanChau.to_csv(path_final+r"Beacon_10.8006_105.2480_WaterLevel_TimeSeries_TanChau.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_Chaktomuk.to_csv(path_final+r"Beacon_11.5629_104.9352_WaterLevel_TimeSeries_Chaktomuk.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_KhongChiam.to_csv(path_final+r"Beacon_15.3220_105.4934_WaterLevel_TimeSeries_KhongChiam.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_Vientien.to_csv(path_final+r"Beacon_17.9309_102.6155_WaterLevel_TimeSeries_Vientien.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')



df_Rainfall_Thoeng.to_csv(path_final+"Beacon_10.8006_105.2480_Rainfall_TimeSeries_Thoeng.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_LuangPrabang.to_csv(path_final+"Beacon_11.5629_104.9352_Rainfall_TimeSeries_LuangPrabang.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_Kontum.to_csv(path_final+"Beacon_15.3220_105.4934_Rainfall_TimeSeries_Kontum.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_Rainfall_KampongAmpil.to_csv(path_final+"Beacon_17.9309_102.6155_Rainfall_TimeSeries_KampongAmpil.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_Thoeng.to_csv(path_final+"Beacon_10.8006_105.2480_WaterLevel_TimeSeries_Thoeng.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_LuangPrabang.to_csv(path_final+"Beacon_11.5629_104.9352_WaterLevel_TimeSeries_LuangPrabang.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_Kontum.to_csv(path_final+"Beacon_15.3220_105.4934_WaterLevel_TimeSeries_Kontum.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')
df_WaterLevel_KampongAmpil.to_csv(path_final+"Beacon_17.9309_102.6155_WaterLevel_TimeSeries_KampongAmpil.csv",index = False,mode='a', header = 0,date_format='%Y-%m-%d')






