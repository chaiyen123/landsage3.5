#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
from datetime import datetime, timedelta,date
import numpy as np
import glob
import csv


# In[ ]:


path = "D:/Landsage/pinmanagerserver-master/data/csv/"
file_select = "Rainfall_TimeSeries"
file_select2= "WaterLevel_TimeSeries"
all_files = glob.glob(path+"*" + file_select +"*.csv")
for f in all_files:
    print (f)
    df = pd.read_csv(f)
    df['Date'] = pd.to_datetime(df['Date'])
    df.to_csv(f, encoding ='utf-8', index =False, date_format='%Y-%m-%d')
    df = pd.read_csv(f, index_col=0)
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    dtr =pd.date_range(df.index.min(), df.index.max(), freq='D')
    s = pd.Series(index=dtr)
    df = pd.concat([df,s[~s.index.isin(df.index)]]).sort_index()
    df = df.drop([0],axis=1).fillna(np.nan).rename_axis('Date').reset_index()
    df.to_csv(f, index =False,encoding ='utf-8', date_format='%Y-%m-%d')

all_files = glob.glob(path+"*" + file_select +"*.csv")
for f  in all_files:
    print (f)
    df = pd.read_csv(f)
    df = df.astype({"Date": str})
    df = df.drop_duplicates(subset='Date', keep="last")
    df.to_csv(f, index =False, date_format='%Y-%m-%d')



# In[ ]:


all_files2= glob.glob(path+"*" + file_select2+"*.csv")    
for f in all_files2:
    print (f)
    df = pd.read_csv(f)
    df['Date'] = pd.to_datetime(df['Date'])
    df['Date'] = df['Date'].dt.strftime('%Y-%m-%d')
    df.to_csv(f, encoding ='utf-8', index =False, date_format='%Y-%m-%d')
    df = pd.read_csv(f, index_col=0)
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    dtr =pd.date_range(df.index.min(), df.index.max(), freq='D')
    s = pd.Series(index=dtr)
    df = pd.concat([df,s[~s.index.isin(df.index)]]).sort_index()
    df = df.drop([0],axis=1).fillna(np.nan).rename_axis('Date').reset_index()
    df.to_csv(f, encoding ='utf-8', index =False, date_format='%Y-%m-%d')

all_files2= glob.glob(path+"*" + file_select2+"*.csv")
for f  in all_files2:
    print (f)
    df = pd.read_csv(f)
    df = df.astype({"Date": str})
    df = df.drop_duplicates(subset='Date', keep="last")
    df.to_csv(f,  index =False, date_format='%Y-%m-%d')
    
    


# In[ ]:





# In[ ]:




