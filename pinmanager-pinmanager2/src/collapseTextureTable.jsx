import React, { Component } from "react";
import Badges from "./common/badge";
import BootstrapTable from "react-bootstrap-table-next";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Box from "@material-ui/core/Box";
import { Tab } from "bootstrap";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import Modalwindow from "./modal";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import _ from "lodash";
import ReactHover, { Trigger, Hover } from "react-hover";
const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);
const optionsCursorTrueWithMargin = {
  followCursor: true,
  shiftX: 20,
  shiftY: 0,
};

class CollapseTexturetable extends Component {
  state = { open: false };
  handleOpen = () => {
    const open = !this.state.open;
    this.setState({ open });
  };
  render() {
    const { post } = this.props;
    post.fileList = _.orderBy(
      post.fileList,
      ["year", "month", "date"],
      ["asc", "asc", "asc"]
    );
    return (
      <React.Fragment>
        <TableRow>
          <StyledCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => this.handleOpen()}
            >
              {this.state.open ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )}
            </IconButton>
          </StyledCell>
          <StyledCell component="th" scope="row">
            {post.textureName}
          </StyledCell>
          <StyledCell align="middle">{post.country}</StyledCell>
          <StyledCell align="middle">{post.mainType}</StyledCell>
          <StyledCell align="middle">{post.subType}</StyledCell>
          <StyledCell align="middle">{post.fileList.length}</StyledCell>
          <StyledCell align="middle">
            <div className="btn-group">
              <Modalwindow
                action="addTextureData"
                originalState={post}
                handleUpdate={this.props.handleUpdate}
              />

              <Modalwindow
                action="editTexture"
                originalState={post}
                originalId={post._id}
                handleUpdate={this.props.handleUpdate}
              />

              <button
                className="btn btn-danger btn-sm ml-3"
                onClick={() =>
                  confirmAlert({
                    title: `Delete confirmation`,
                    message: `Are you cure to delete \"${post.textureName}\"?`,
                    buttons: [
                      {
                        label: "Yes",
                        onClick: () => {
                          this.props.handleDelete(post);
                        },
                      },
                      {
                        label: "No",
                        onClick: () => {},
                      },
                    ],
                    "&.react-confirm-alert-overlay": {
                      zIndex: 99999,
                    },
                  })
                }
              >
                Delete
              </button>
            </div>
          </StyledCell>
        </TableRow>
        <TableRow>
          <StyledCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  Data List
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <StyledCell>FileName</StyledCell>
                      <StyledCell>Date</StyledCell>
                      <StyledCell>Month</StyledCell>
                      <StyledCell>Year</StyledCell>
                      <StyledCell>Legend File</StyledCell>
                      <StyledCell>North</StyledCell>
                      <StyledCell>South</StyledCell>
                      <StyledCell>East</StyledCell>
                      <StyledCell>West</StyledCell>
                      <StyledCell>Edit</StyledCell>
                      <StyledCell>Delete</StyledCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {post.fileList.map((file) => (
                      <TableRow key={file.textureFilename}>
                        <StyledCell component="th" scope="row">
                          <ReactHover options={optionsCursorTrueWithMargin}>
                            <Trigger type="trigger">
                              <a
                                href={`http://localhost:5000/data/texture/${file.textureFilename}`}
                                download={file.textureFilename}
                              >
                                <p>{file.textureFilename} </p>
                              </a>
                            </Trigger>
                            <Hover type="hover">
                              <img
                                className={"thumbnail"}
                                alt="Albert Einstein"
                                src={`http://localhost:5000/data/texture/${file.textureFilename}`}
                                height="400"
                              />
                            </Hover>
                          </ReactHover>
                        </StyledCell>
                        <StyledCell>{file.date}</StyledCell>
                        <StyledCell>{file.month}</StyledCell>
                        <StyledCell>{file.year}</StyledCell>
                        <StyledCell>
                          <ReactHover options={optionsCursorTrueWithMargin}>
                            <Trigger type="trigger">
                              <a
                                href={`http://localhost:5000/data/texture/${file.legendFilename}`}
                                download={file.legendFilename}
                              >
                                <p>{file.legendFilename} </p>
                              </a>
                            </Trigger>
                            <Hover type="hover">
                              <img
                                className={"thumbnail"}
                                alt="Albert Einstein"
                                src={`http://localhost:5000/data/texture/${file.legendFilename}`}
                                height="400"
                                width="300"
                              />
                            </Hover>
                          </ReactHover>
                        </StyledCell>
                        <StyledCell>{file.border.N}</StyledCell>
                        <StyledCell>{file.border.S}</StyledCell>
                        <StyledCell>{file.border.E}</StyledCell>
                        <StyledCell>{file.border.W}</StyledCell>

                        <StyledCell align="middle">
                          <Modalwindow
                            action="editTextureData"
                            originalState={post}
                            originalFile={file}
                            handleUpdate={this.props.handleUpdate}
                          />
                        </StyledCell>
                        <StyledCell align="middle">
                          <button
                            className="btn btn-danger btn-sm"
                            onClick={() =>
                              confirmAlert({
                                title: `Delete confirmation`,
                                message: `Are you cure to delete \"${file.textureFilename}\"?`,
                                buttons: [
                                  {
                                    label: "Yes",
                                    onClick: () => {
                                      this.props.handleDelData(post, file);
                                    },
                                  },
                                  {
                                    label: "No",
                                    onClick: () => {},
                                  },
                                ],
                                "&.react-confirm-alert-overlay": {
                                  zIndex: 99999,
                                },
                              })
                            }
                          >
                            Delete
                          </button>
                        </StyledCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </StyledCell>
        </TableRow>
      </React.Fragment>
    );
  }
}
export default CollapseTexturetable;
