import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
import Progress from "../common/progress";
//import { mainType, subType, visualFormat } from "../DataTypeSelector.js";
import Wheel from "../WheelList.json";
import config from "../config";
//component for render form inside modal
class AddTextureForm extends Form {
  state = {
    data: [],
    dataHeader: {
      textureName: "",
      country: "",
      maintype: "Landslide",
      subtype: "",
      date: "",
      month: "",
      year: "",
      N: "",
      S: "",
      E: "",
      W: "",
      fileName: "",
      legendFilename: "",
      maincustom: "",
    },
    errors: {},
    uploadPercentage1: 0,
    uploadPercentage2: 0,
    existData: [],
    Wheel: "",
  };
  async componentDidMount() {
    const { data: Wheel } = await http.get(config.wheelEndpoint);
    this.setState({ Wheel });
    //console.log(Wheel);
  }
  //define data schema
  schema = {
    textureName: Joi.string().required().label("Texture name"),
    country: Joi.string().required().label("Country"),
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub DataType"),
    date: Joi.number().min(1).max(31).required().label("Date"),
    month: Joi.number().min(1).max(12).required().label("Month"),
    year: Joi.number().min(2000).max(2070).required().label("Year"),
    N: Joi.number().min(-180).max(180).required().label("North"),
    E: Joi.number().min(-180).max(180).required().label("East"),
    S: Joi.number().min(-180).max(180).required().label("South"),
    W: Joi.number().min(-180).max(180).required().label("West"),
    fileName: Joi.string().required(),
    legendFilename: Joi.string().required(),
    maincustom: Joi.string().allow(""),
  };
  // function for posting data(add station)
  handleUpload = async (file, type) => {
    //console.log(file);
    if (type === "MapFile") {
      const dataHeader = {
        ...this.state.dataHeader,
        fileName: file.name,
      };
      this.setState({ dataHeader });
    } else if (type === "LegendFile") {
      const dataHeader = {
        ...this.state.dataHeader,
        legendFilename: file.name,
      };
      this.setState({ dataHeader });
    }

    // auto set input value from file name
    // let fileNameSplit = file.name.replace(/\.[^/.]+$/, "").split("_");
    // console.log(fileNameSplit);
    // if (fileNameSplit.length === 5) {
    //   const autoHeader = {
    //     ...this.state.dataHeader,
    //     name: fileNameSplit[4],
    //     lat: fileNameSplit[1],
    //     long: fileNameSplit[2],
    //   };
    //   this.setState({ dataHeader: autoHeader });
    // }
    //this.setState({ uploadPercentage: 0 });
    const formData = new FormData();
    formData.append("file", file);
    //upload csv file to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
        onUploadProgress: (progressEvent) => {
          //update progress bar state
          if (type === "MapFile") {
            const uploadPercentage1 =
              (progressEvent.loaded / progressEvent.total) * 100;

            this.setState({ uploadPercentage1 });
            setTimeout(() => this.setState({ uploadPercentage1: 0 }), 1000);
          } else {
            const uploadPercentage2 =
              (progressEvent.loaded / progressEvent.total) * 100;

            this.setState({ uploadPercentage2 });
            setTimeout(() => this.setState({ uploadPercentage2: 0 }), 1000);
          }
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  doSubmit = () => {
    let filetype = "";
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") ||
        this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";
        break;
      case this.state.dataHeader.fileName.includes(".png") ||
        this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";
        break;
      default:
        filetype = "csv";
    }
    const payload = {
      textureName: this.state.dataHeader.textureName,
      country: this.state.dataHeader.country,
      mainType:
        this.state.dataHeader.maintype === "Custom"
          ? this.state.dataHeader.maincustom
          : this.state.dataHeader.maintype,
      subType: this.state.dataHeader.subtype,
      fileList: [
        {
          textureFilename: this.state.dataHeader.fileName,
          legendFilename: this.state.dataHeader.legendFilename,
          date: this.state.dataHeader.date,
          month: this.state.dataHeader.month,
          year: this.state.dataHeader.year,
          fileType: filetype,
          border: {
            N: this.state.dataHeader.N,
            S: this.state.dataHeader.S,
            E: this.state.dataHeader.E,
            W: this.state.dataHeader.W,
          },
        },
      ],
    };
    //upload data to the database
    //console.log(payload);
    this.props.handleAdd(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          this.state.Wheel.subType.Flooding
        );
      case "Landslide":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          this.state.Wheel.subType.Landslide
        );
      case "Custom":
        return (
          <div>
            {this.renderInput("maincustom", "Custom Main DataType")}
            {this.renderInput("subtype", "Custom Sub DataType")}
          </div>
        );
    }
  }
  renderVisualtype(fileName) {
    //console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv") || fileName.includes(".CSV"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.timeSeries
        );
      case fileName.includes(".png") || fileName.includes(".PNG"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.textureMap
        );
    }
  }
  render() {
    //console.log(this.validate());
    //console.log(this.state.dataHeader.fileName);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput(
            "textureName",
            "Layer name",
            "Layer name",
            "Enter Layer Name"
          )}
          {this.renderSelect("country", "Country", Wheel.country)}
          {this.renderSelect(
            "maintype",
            "Main Data Type",
            Wheel.mainType,
            true
          )}
          {this.state.Wheel !== "" ? this.rendersubSelect() : null}
          <h6>Border</h6>
          <div className="form-row">
            <div className="form-group col-3">
              {this.renderInput("N", "North", "North", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("S", "South", "South", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("E", "East", "East", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("W", "West", "West", "")}
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-3">
              {this.renderInput("date", "Date", "Text", "dd")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("month", "Month", "Text", "mm")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("year", "Year", "Text", "yyyy")}
            </div>
          </div>
          {/* conditional rendering progress bar */}
          <FileUpload type="MapFile" handleUpload={this.handleUpload} />
          {this.state.uploadPercentage1 === 0 ? null : (
            <Progress percentage={this.state.uploadPercentage1} />
          )}
          <FileUpload type="LegendFile" handleUpload={this.handleUpload} />
          {this.state.uploadPercentage2 === 0 ? null : (
            <Progress percentage={this.state.uploadPercentage2} />
          )}

          <button
            style={{ margin: 10 }}
            disabled={this.validate() !== null}
            className="btn btn-primary float-right"
          >
            Confirm
          </button>
        </form>
      </div>
    );
  }
}

export default AddTextureForm;
