import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
import Wheel from "../WheelList.json";
import config from "../config";
// extend the rendering input and validate function from Form component
class UpdateTextureForm extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalId,
    data: [],
    dataHeader: {
      textureName: this.props.originalState.textureName,
      country: this.props.originalState.country,
      mainType: this.props.originalState.mainType,
      subType: this.props.originalState.subType,
      maincustom: "",
    },
    errors: {},
    Wheel: "",
  };
  async componentDidMount() {
    const { data: Wheel } = await http.get(config.wheelEndpoint);
    this.setState({ Wheel });
    //console.log(Wheel);
  }
  // define schema for validate input data
  schema = {
    textureName: Joi.string().required().label("Texture Name"),
    country: Joi.string().required().label("Country"),
    mainType: Joi.string().required().label("mainType"),
    subType: Joi.string().required().label("subType"),
    maincustom: Joi.string().allow(""),
  };
  // function for upload csv file
  handleUpload = async (file) => {
    const dataHeader = {
      ...this.state.dataHeader,
    };
    //console.log(dataHeader);
    this.setState({ dataHeader });

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    //console.log("Submitted");
    let payload = {
      ...this.props.originalState,
      textureName: this.state.dataHeader.textureName,
      country: this.state.dataHeader.country,
      mainType: this.state.dataHeader.mainType,
      subType: this.state.dataHeader.subType,
    };
    //console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    switch (this.state.dataHeader.mainType) {
      case "Flooding":
        return this.renderSelect(
          "subType",
          "Sub DataType",
          this.state.Wheel.subType.Flooding
        );
      case "Landslide":
        return this.renderSelect(
          "subType",
          "Sub DataType",
          this.state.Wheel.subType.Landslide
        );
      case "Custom":
        return (
          <div>
            {this.renderInput("maincustom", "Custom Main DataType")}
            {this.renderInput("subtype", "Custom Sub DataType")}
          </div>
        );
    }
  }
  render() {
    //console.log(this.props);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("textureName", "Texture name")}
          {this.renderSelect("country", "Country", Wheel.country)}
          {this.renderSelect("mainType", "Main DataType", Wheel.mainType, true)}
          {this.state.Wheel !== "" ? this.rendersubSelect() : null}
          {this.renderButton("Update")}
        </form>
      </div>
    );
  }
}

export default UpdateTextureForm;
