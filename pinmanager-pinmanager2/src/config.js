const endPoint = {
  URL: process.env.REACT_APP_URL,
  ENV: process.env.REACT_APP_ENV,
};
const config = {
  stationEndpoint: endPoint.URL + "/data/beacon",
  textureEndpoint: endPoint.URL + "/texture",
  fileEndpoint: endPoint.URL + "/file",
  dataEndpoint: endPoint.URL + "/data/",
  wheelEndpoint: endPoint.URL + "/wheel",
};
export default config;
