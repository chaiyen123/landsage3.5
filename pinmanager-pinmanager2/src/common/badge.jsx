import React, { Component } from "react";

class Badges extends Component {
  state = {
    mainType: this.props.mainType,
    subType: this.props.subType,
  };
  renderBadge() {
    switch (this.state.mainType) {
      case "Flooding":
        return (
          <h5>
            <span className="badge badge-pill badge-primary">
              {this.state.subType}
            </span>
          </h5>
        );
      case "Landslide":
        return (
          <h5>
            <span className="badge badge-pill badge-warning">
              {this.state.subType}
            </span>
          </h5>
        );
      default:
        return (
          <h5>
            <span className="badge badge-pill badge-light">
              {this.state.subType}
            </span>
          </h5>
        );
    }
  }
  render() {
    return this.renderBadge();
  }
}

export default Badges;
