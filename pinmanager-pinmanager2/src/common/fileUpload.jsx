import React, { Fragment, useState, useEffect } from "react";
import http from "../services/httpService";
//prompt alert
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import config from "../config";

export const FileUpload = (props) => {
  const [file, setFile] = useState(null);
  const [filename, setFilename] = useState(
    props.originalfileName ? props.originalfileName : "Choose File"
  );
  const [fileList, setFileList] = useState([]);
  useEffect(() => {
    async function fetchMyAPI() {
      const { data: posts } = await http.get(config.dataEndpoint);
      setFileList(posts.fileList);
      //console.log(posts);
    }
    fetchMyAPI();
  }, []);
  const onChange = (e) => {
    console.log(e.target.files[0]);
    const data = e.target.files[0];
    setFilename(e.target.files[0].name);
    if (fileList.includes(e.target.files[0].name)) {
      confirmAlert({
        title: `File Named "${e.target.files[0].name}" already exist in the server`,
        message: "Please change file name and try again",
        buttons: [
          // {
          //   label: "Yes",
          //   onClick: () => {
          //     props.handleUpload(data);
          //   },
          // },
          // {
          //   label: "No",
          //   onClick: () => {
          //     setFile("");
          //     setFilename("Choose File");
          //   },
          // },
          {
            label: "Ok",
            onClick: () => {
              setFile("");
              setFilename("Choose File");
            },
          },
        ],
        "&.react-confirm-alert-overlay": { zIndex: 99999 },
      });
    } else {
      props.type
        ? props.handleUpload(data, props.type)
        : props.handleUpload(data);
    }
    e.target.value = null;
  };
  return (
    <Fragment>
      <p> {props.type}</p>
      <div className="custom-file mb-4">
        <input
          type="file"
          ref={this.uploadField}
          accept={props.type ? ".png" : ".csv"}
          className="custom-file-input"
          id="customFile"
          onChange={onChange}
        />
        <label className="custom-file-label" htmlFor="customFile">
          {filename}
        </label>
      </div>
    </Fragment>
  );
};
