import axios from "axios";
import React, { Component } from "react";
import LineChartPlotly from "./linechart-plotly";
import { io } from "socket.io-client";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Sponser from "./sponser";
const randomColor = require("randomcolor");

class DrawChartFromUrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      socketServerURL: `http://${window.location.hostname}:8080`,
      dataServerURL: `http://localhost:${this.props.targetPort}/data/csv/`,
      filename: this.props.targetFilename,
      dataJsonStr: "",
      targetProperty: this.props.targetProperty,
      uiRevisionNo: 0,
      isSetRange: false,
      xAxisRange: [],
      targetPort: this.props.targetPort,
      socket: null,
      siteProfiles: ["GLOBAL", "TH", "VN", "LA", "KH"],
      currentSite: "GLOBAL",
      isSync: true,
      isLocalSync: true,
      dateXRangeSlider: null,
      isFillFrame: this.props.isFillFrame ? this.props.isFillFrame : false,
      isCustomColor: false,
      customColor:"#191919"
    };
  }

  updateXAxisRange = (dateXRangeSlider) => {
    //this.setState({ dateXRangeSlider: newSendDateXRangeSlider });
    // console.log("Updating xAxis Range");
    // console.log("Check unchange value");

    if (dateXRangeSlider) {
      //const xRangeslider = dateXRangeSlider["xrange"];
      if (this.state.dateXRangeSlider === dateXRangeSlider) {
        // console.log("Data is the same - return");
        // console.log(this.state.dateXRangeSlider);
        // console.log(dateXRangeSlider);
        return;
      } else {
        // console.log("Data is different");
        // console.log(this.state.dateXRangeSlider);
        // console.log(dateXRangeSlider);
      }
    } else {
      // console.log("Data is not defined - return");
      return;
    }

    if (
      dateXRangeSlider &&
      this.state.isSync && // Work on update component on sync only
      this.state.isLocalSync
    ) {
      // console.log(
      //   "Checked: this.state.targetDateXRangeSlider has been updated"
      // );
      const xRangeslider = dateXRangeSlider["xrange"];
      if (xRangeslider) {
        //console.log("Update Graph Range based on new input");
        //console.log(xRangeslider);
        const xminDate = new Date(xRangeslider[0]);
        const xmaxDate = new Date(xRangeslider[1]);
        //console.log(xminDate.toISOString().split("T")[0]);
        //console.log(xmaxDate.toISOString().split("T")[0]);
        this.setState((prevState, props) => { return {
          isSetRange: true,
          dateXRangeSlider: dateXRangeSlider,
          xAxisRange: [
            xminDate.toISOString().split("T")[0],
            xmaxDate.toISOString().split("T")[0],
          ],
          uiRevisionNo: prevState.uiRevisionNo + 1,
        }});
      }
    }
  };

  async componentDidMount() {
    this.initSocketConnection();
    this.requestDataFromServer();
  }

  componentWillUnmount() {
    if (this.state.socket) {
      if (this.state.socket.connected) {
        //console.log("disconnect on dismount");
        // Disconnect on dismount
        this.state.socket.disconnect();
      }
    }
  }

  initSocketConnection = () => {
    if (this.state.socket === null) {
      //console.log("Init Socket Connection");
      const newSocket = io(this.state.socketServerURL);
      newSocket.on("connect", () => {
        //console.log(`socket id = ${newSocket.id}`); // "G5p5..."
        //console.log(`connected to ${this.state.socketServerURL}`);

        //console.log(`JOIN profile: ${this.state.siteProfiles[0]}`);
        newSocket.emit("JOIN", this.state.siteProfiles[0]);
        this.setState(  {currentSite: this.state.siteProfiles[0] });
        //setCurrentSite(this.state.siteProfiles[0]);
        //setCurrentSite("DUDE");
      });
      // Check for sync Event
      newSocket.on("SEND_SYNC", (newSyncStatus) => {
        // Sync data
        //console.log(`[ON SEND_SYNC]: ${newSyncStatus}`);
        if (this.state !== newSyncStatus) {
          //console.log("New isSync value - updated");
          this.setState({
            isSync: newSyncStatus,
          });
        } else {
          //console.log("Same isSync value - no update");
        }
      });

      // Check for Date Range - Deprecated : not use by graph app but web-app
      newSocket.on("SEND_CHANGE_DATE_RANGE", this.OnChangeDateRangeEvent);

      // Check for Change Date XRange Slider Event
      newSocket.on(
        "SEND_CHANGE_DATE_XRANGE_SLIDER",
        this.OnChangeDateXRangeSliderEvent
      );

      newSocket.on("disconnect", () => {
        //console.log(`disconnected from ${this.state.socketServerURL} `);
      });
      // Register Socket after ini the event listener
      this.setState({ socket: newSocket });
      //setSocket(newSocket);
    }
  };

  requestDataFromServer = async () => {
    //console.log("Data Request from server");
    const targetUrl = this.state.dataServerURL.concat(this.state.filename);
    //("http://localhost:5000/json/Beacon_17.928_102.620_TimeSeries_Vientien");
    //targetUrl = "https://jsonplaceholder.typicode.com/todos/1";

    const response = await axios.get(targetUrl).catch((error) => {
      this.setState({ isLoaded: false, error: "Error Requesting data" });
      //console.log(error);
    });

    if (response) {
      //console.log("Received new data response");
      //console.log(response.data);
      const jsonStringified = response.data;
      //console.log(jsonStringified);
      this.setState({ isLoaded: true, dataJsonStr: jsonStringified });
    }
  };

  OnChangeDateRangeEvent = (newSendDateRange) => {
    // console.log(
    //   `[ON SEND_CHANGE_DATE_RANGE]: ${newSendDateRange}}-LocalSync:${this.state.isLocalSync}`
    // );
    // console.log(
    //   "Ignored [ON SEND_CHANGE_DATE_RANGE] - use XRangeSlider instead"
    // );
    //if (this.state.isLocalSync === true) setDateRange(newSendDateRange);
  };

  OnChangeDateXRangeSliderEvent = (newSendDateXRangeSlider) => {
    // console.log(
    //   `[ON SEND_CHANGE_DATE_XRANGE_SLIDER]: ${newSendDateXRangeSlider}-LocalSync:${this.state.isLocalSync}`
    // );
    // console.log(newSendDateXRangeSlider);
    if (this.state.isLocalSync === true) {
      //this.setState({ dateXRangeSlider: newSendDateXRangeSlider });
      this.updateXAxisRange(newSendDateXRangeSlider);
    }
  };

  toggleIsSync = () => {
    // console.log(
    //   `>>Handle Toggle isSync from ${this.state.isSync} to ${!this.state
    //     .isSync}`
    // );
    // Use socket to emit event of new date back to socket server
    const socket = this.state.socket;
    if (socket) {
      if (socket.connected) {
        socket.emit("SYNC", !this.state.isSync);
        this.setState((prevState, props) => { return { isSync: !prevState.isSync }});
      } else {
        // console.log("Socket isn't connected");
      }
    }
  };

  toggleIsLocalSync = () => {
    // console.log(
    //   `>>Handle Toggle isLocalSync from ${this.state.isLocalSync} to ${!this
    //     .state.isLocalSync}`
    // );
    // Use socket to emit event of new date back to socket server
    this.setState((prevState, props) => {return { isLocalSync: !prevState.isLocalSync }});
  };

  onProfileSwitch = (event) => {
    // console.log("Check box checked");

    const target = event.target;
    // console.log(target.id);
    // console.log(target.name);
    // console.log(target.value);
    const socket = this.state.socket;
    if (socket && event) {
      // console.log(`JOIN Profile:${target.value}`);
      socket.emit("JOIN", target.value);
      this.setState({ currentSite: target.value });
    }
  };

  handleToggleIsFillFrame = () => {
    // console.log(`Toggle is fill frame to ${!this.state.isFillFrame}`);
    this.setState((prevState, props) => {return { isFillFrame: !prevState.isFillFrame }});
  };

  handleSetRangeManually = (newRange) => {
    // console.log("Set new Range to " + newRange);
    //pattern = "d{4}-d{2}-d{2}";
    if(!newRange[0].match(/^\d{4}-\d{1,2}-\d{1,2}$/g) || !newRange[1].match(/^\d{4}-\d{1,2}-\d{1,2}$/g))
    {
      // console.log("Invalid range - ignored");
      return;
    }
    if (Array.isArray(newRange)) {
      if (newRange.length >= 2) {
        // console.log("Revision change to ->" + (this.state.uiRevisionNo + 1));
        // Trigger redraw the chart
        this.setState((prevState, props) => {return {
          uiRevisionNo: prevState.uiRevisionNo + 1,
          isSetRange: true,
          xAxisRange: newRange,
        }});

        // Broadcast to socket server
        const d1msecs = new Date(newRange[0]).getTime(); // get milliseconds
        const d2msecs = new Date(newRange[1]).getTime(); // get milliseconds

        const avgTime = (d1msecs + d2msecs) / 2;
        //console.log(avgTime);

        const result = new Date(avgTime);
        //console.log(`date in middle is ${result.toISOString().split("T")[0]}`);
        this.handleSocketChangeDateRange(result.toISOString().split("T")[0]);
        this.handleSocketChangeDateXRangeSlider({ xrange: newRange });
      } else {
        console.error("newRange does not have at least 2 members in array");
      }
    } else {
      console.error("Error: newRange is not an array");
    }
    //console.log("Update Parent State");
    //console.log(this.state);
  };

  handleSocketChangeDateRange = (newDate) => {
    if (!this.state.isSync) {
      // Work on update component on sync only
      return;
    }

    if (!this.state.isLocalSync) {
      // Work on update componenet on isLocal Sync only
      return;
    }

    //console.log(`>>Handle Socket change Date Range with ${newDate}`);
    // Use socket to emit event of new date back to socket server
    const socket = this.state.socket;
    if (socket) {
      if (socket.connected) {
        //console.log("CHANGE_DATE_RANGE emit");
        socket.emit("CHANGE_DATE_RANGE", newDate);
      } else {
        //console.log("Socket isn't connected");
      }
    }
  };

  handleSocketChangeDateXRangeSlider = (newDateRange) => {
    if (!this.state.isSync) {
      // Work on update component on sync only
      return;
    }

    if (!this.state.isLocalSync) {
      // Work on update componenet on isLocal Sync only
      return;
    }

    // Expect to be a class with an array of length 2, [0] for min x-range, and [1] for max x-range
    // {xrange:[]}
    // console.log(
    //   `>>Handle Socket change Date XRange Slider with ${newDateRange}`
    // );
    // console.log(newDateRange);
    // Use socket to emit event of new date back to socket server
    const socket = this.state.socket;
    if (socket) {
      if (socket.connected) {
        if (newDateRange) {
          //console.log("CHANGE_DATE_XRANGE_SLIDER emit");
          socket.emit("CHANGE_DATE_XRANGE_SLIDER", newDateRange);
        }
      } else {
        //console.log("Socket isn't connected");
      }
    }
  };

  handleRandomNewColor = () => {
    const newRandomColor = randomColor();
    this.setState({ isCustomColor: true, customColor: newRandomColor });
  }

  checkIsJsonString(str) {
    try {
      return JSON.parse(str);
    } catch (e) {
      return false;
    }
  }

  createLineChartComponent = (dataJSONObj) => {
    //console.log("Rendered - createLineChartComponent!");
    //console.log(this.state);
    return (
      <div className="drawchart">
        <Button
          variant="secondary"
          size="sm"
          id="syncBtn"
          onClick={this.toggleIsSync}
        >
          {this.state.isSync ? "Un-sync" : "Sync"}
        </Button>{" "}
        <Form className="d-inline">
          <Form.Check
            checked={this.state.isLocalSync}
            inline
            type="switch"
            id="custom-switch-sync"
            label="Sync this window"
            //checked
            onChange={this.toggleIsLocalSync}
          />
        </Form>
        <Form className="d-inline">
          <div key={"inline-radio"} className="d-inline">
            <Form.Check
              //checked
              inline
              label="Global"
              value="GLOBAL"
              name="countryGroup"
              type="radio"
              id="profile-GLOBAL"
              //defaultChecked
              defaultChecked={
                this.state.currentSite === this.state.siteProfiles[0]
                  ? true
                  : false
              }
              //["GLOBAL", "TH", "VN", "MM", "KH"]
              onChange={this.onProfileSwitch}
            />
            <Form.Check
              inline
              label="TH"
              value="TH"
              name="countryGroup"
              type="radio"
              id="profile-TH"
              defaultChecked={
                this.state.currentSite === this.state.siteProfiles[1]
                  ? true
                  : false
              }
              //["GLOBAL", "TH", "VN", "MM", "KH"]
              onChange={this.onProfileSwitch}
            />
            <Form.Check
              inline
              label="VN"
              value="VN"
              name="countryGroup"
              type="radio"
              id="profile-VN"
              defaultChecked={
                this.state.currentSite === this.state.siteProfiles[2]
                  ? true
                  : false
              }
              //["GLOBAL", "TH", "VN", "MM", "KH"]
              onChange={this.onProfileSwitch}
            />
            <Form.Check
              inline
              label="LA"
              value="LA"
              name="countryGroup"
              type="radio"
              id="profile-LA"
              defaultChecked={
                this.state.currentSite === this.state.siteProfiles[3]
                  ? true
                  : false
              }
              //["GLOBAL", "TH", "VN", "MM", "KH"]
              onChange={this.onProfileSwitch}
            />
            <Form.Check
              inline
              label="KH"
              value="KH"
              name="countryGroup"
              type="radio"
              id="profile-KH"
              defaultChecked={
                this.state.currentSite === this.state.siteProfiles[4]
                  ? true
                  : false
              }
              //["GLOBAL", "TH", "VN", "MM", "KH"]
              onChange={this.onProfileSwitch}
            />
          </div>
        </Form>
        <Sponser isShownSponser={true} />
        <LineChartPlotly
          //isSync={this.state.isSync}
          //isLocalSync={this.state.isLocalSync}
          isFillFrame={this.state.isFillFrame}
          dataJson={dataJSONObj}
          dataSource={this.state.dataServerURL.concat(this.state.filename)}
          dataPropertyName={this.state.targetProperty}
          handleSetRange={this.handleSetRangeManually}
          uiRevisionNo={this.state.uiRevisionNo}
          isSetRange={this.state.isSetRange}
          xAxisRange={this.state.xAxisRange}
          handleRefreshServerData={this.requestDataFromServer}
          handleSocketChangeDateRange={this.handleSocketChangeDateRange}
          handleSocketChangeDateXRangeSlider={
            this.handleSocketChangeDateXRangeSlider
          }
          handleToggleIsFillFrame={this.handleToggleIsFillFrame}
          handleRandomNewColor={this.handleRandomNewColor}
          isCustomColor={this.state.isCustomColor}
          customColor={this.state.customColor}
        />
      </div>
    );
  };

  renderGraphFromJsonData = () => {
    const { isLoaded, dataJsonStr, error } = this.state;
    //console.log("isLoaded = " + isLoaded);

    if (isLoaded) {
      var dataJSONObj;
      //========= Validation 1 json=================
      if (this.checkIsJsonString(dataJsonStr)) {
        //console.log("Correct Json data string");
        //console.log(dataJsonStr);
        try {
          // Parse and use the result to render
          dataJSONObj = JSON.parse(dataJsonStr);
          return this.createLineChartComponent(dataJSONObj);
        } catch (error) {
          return (
            <div>
              <p>Error parsing json string data {error}</p>
            </div>
          );
        }
      } else {
        //console.log("Incorrect Json data [String]");
        //console.log(dataJsonStr);
        //console.lo("Check if Json data [object]");
        if (dataJsonStr.data !== undefined) {
          // By-pass JSON.Parse and use it as json object directly
          // console.log(
          //   `It is Json data Object with size: ${dataJsonStr.data.length}`
          // );
          // console.log(`Create Line Chart Componenet now !`);
          dataJSONObj = dataJsonStr;
          return this.createLineChartComponent(dataJSONObj);
        } else {
          return (
            <p>
              Error invalid data to render. Please check url whether it returns
              a correct json data
            </p>
          );
        }
      }
    } else if (error) {
      // Loading error?
      return (
        <div>
          <p>Error during loading...{error}</p>
        </div>
      );
    } else {
      // Not isLoaded = true
      return (
        <div>
          <p>Loading...</p>
        </div>
      );
    }
  };

  render() {
    return (
      <div className="graph-visualizer-container">
        {this.renderGraphFromJsonData()}
      </div>
    );
  }
}

export default DrawChartFromUrl;
